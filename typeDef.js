const { gql } = require('apollo-server')

// Type definitions define the "shape" of your data and specify
// which ways the data can be fetched from the GraphQL server.
const typeDefs = gql`
  # Comments in GraphQL are defined with the hash (#) symbol.

  type User {
    name: String
    password: String
  }

  input UserInput {
    name: String
    password: String
  }
  
  # The "Query" type is the root of all GraphQL queries.
  # (A "Mutation" type will be covered later on.)
  type Query {
    users: [User]
  }
  type Mutation {
    saveUser(name: String, password: String): String
    removeUser(name: String) : String
  }
`;

module.exports = typeDefs;