var mongoose = require('mongoose');
const { dbUrl, dbName } = require('./config')
// mongoose.connect('mongodb://localhost:27017/dentsu-demo');
mongoose.connect(`${dbUrl}/${dbName}`);

mongoose.connection.on('error', (err) => {
  console.log('Mongo Err', err);
}).on('connected', () => {
  console.log('Connected Successfully');
})
