const User = require('./model/user')

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
const resolvers = {
  Query: {
    users: () => User.find(),
  },
  Mutation: {
    saveUser: async (root, args, context, info) => {
      const {name, password } = args;
    //  return saveUser(args.name, args.password)
     await new User({
      name,
      password
    }).save();
   return `${name}_${new Date().getTime()}`;
    },

    removeUser: async (root, args, context, info) => {
      await User.findOneAndRemove({
        name: args.name
      })
      return 'item delete successfully'
    }
  }
};

saveUser = async (name, password) => {
  const err = await new User({
    name,
    password
  }).save();
 return `${name}_${new Date().getTime()}`;
}

 module.exports = resolvers